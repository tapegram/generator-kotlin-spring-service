"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-kotlin-spring-service:app", () => {
  beforeAll(() => {
    return helpers
      .run(path.join(__dirname, "../generators/app"))
      .withPrompts({ projectName: "newProject" });
  });

  it("creates gradle files", () => {
    assert.file([
      "newProject/build.gradle",
      "newProject/gradle.properties",
      "newProject/settings.gradle",
      "newProject/gradle/wrapper/gradle-wrapper.jar",
      "newProject/gradle/wrapper/gradle-wrapper.properties",
      "newProject/gradlew",
      "newProject/gradlew.bat"
    ]);
  });

  it("creates gradle dependencies files", () => {
    assert.file([
      "newProject/buildSrc/src/main/kotlin/com/onepeloton/dependencies/Dependencies.kt"
    ]);
  });
});
