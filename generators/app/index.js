"use strict";
const Generator = require("yeoman-generator");
const chalk = require("chalk");
const yosay = require("yosay");

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(
        `Welcome to the cool ${chalk.red(
          "Kotlin service with Spring Boot"
        )} generator!`
      )
    );

    const prompts = [
      {
        type: "input",
        name: "projectName",
        message: "Your project name",
        default: this.appname // Default to current folder name
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    this.fs.copyTpl(
      this.templatePath(".gradle/"),
      this.destinationPath(`${this.props.projectName}/.gradle/`),
      this.props
    );
    this.fs.copyTpl(
      this.templatePath("buildSrc/"),
      this.destinationPath(`${this.props.projectName}/buildSrc/`),
      this.props
    );
    this.fs.copyTpl(
      this.templatePath("gradle/"),
      this.destinationPath(`${this.props.projectName}/gradle/`),
      this.props
    );
    this.fs.copyTpl(
      this.templatePath("build.gradle"),
      this.destinationPath(`${this.props.projectName}/build.gradle`),
      this.props
    );
    this.fs.copyTpl(
      this.templatePath("gradle.properties"),
      this.destinationPath(`${this.props.projectName}/gradle.properties`),
      this.props
    );
    this.fs.copyTpl(
      this.templatePath("gradlew"),
      this.destinationPath(`${this.props.projectName}/gradlew`),
      this.props
    );
    this.fs.copyTpl(
      this.templatePath("gradlew.bat"),
      this.destinationPath(`${this.props.projectName}/gradlew.bat`),
      this.props
    );
    this.fs.copyTpl(
      this.templatePath("settings.gradle"),
      this.destinationPath(`${this.props.projectName}/settings.gradle`),
      this.props
    );
  }

  install() {
    // This.installDependencies();
  }
};
