# generator-kotlin-spring-service [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Kotlin Service with Spring Boot

## Installation

First, install [Yeoman](http://yeoman.io) and generator-kotlin-spring-service using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-kotlin-spring-service
```

Then generate your new project:

```bash
yo kotlin-spring-service
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Tavish Pegram]()


[npm-image]: https://badge.fury.io/js/generator-kotlin-spring-service.svg
[npm-url]: https://npmjs.org/package/generator-kotlin-spring-service
[travis-image]: https://travis-ci.com//generator-kotlin-spring-service.svg?branch=master
[travis-url]: https://travis-ci.com//generator-kotlin-spring-service
[daviddm-image]: https://david-dm.org//generator-kotlin-spring-service.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-kotlin-spring-service
[coveralls-image]: https://coveralls.io/repos//generator-kotlin-spring-service/badge.svg
[coveralls-url]: https://coveralls.io/r//generator-kotlin-spring-service
